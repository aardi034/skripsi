<?php
	include 'header.php';
	include 'config.php';

if(isset($_SESSION['email'])){    
?>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Produk</td>
							<td class="description">Detail</td>
							<td class="price">harga</td>
							<td class="">Action</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$session 	=$_SESSION['email'];
						$sql_cart = mysqli_query($con, "SELECT * FROM pembeli WHERE email='$session' ") or die (mysqli_error($con));
					     $get_cart = mysqli_fetch_array($sql_cart);
					    $id_pembeli	= $get_cart['id_pembeli'];


						$query = "SELECT * FROM cart INNER JOIN iklan ON cart.id_iklan = iklan.id_iklan WHERE id_pembeli='$id_pembeli' ";

						 $sql_c = mysqli_query($con,$query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_c) > 0)
            				while($data = mysqli_fetch_array($sql_c)) {
						?>
						<tr>
							<td class="cart_product">
								<img src="../images/<?php echo $data['gambar'];?>" border="0" width="150px" height="150px"/>
							</td>
							<td class="cart_description">
								<?=$data['deskripsi']?>
								<p><?php
                      $id     = $data['id_kat'];
                  $sql_kategori = mysqli_query($con," SELECT * FROM kategori WHERE id_kat='$id' "); 
                  $kategori   = mysqli_fetch_array($sql_kategori);
                  echo $kategori['nama'];
                ?></p>
							</td>
							<td class="cart_price">
								<p><?=$data['harga']?></p>
							</td>
							
							<!-- </td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td> -->
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="cart-delete.php?id=<?=$data['id_cart']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini Dari Keranjang Belanja ? ')"><i class="fa fa-times"></i></a>
							</td>
						</tr>
		<?php
					} else {
						?>
							<?php echo "<script>alert('Keranjang Belanja Anda Masih Kosong, Silahkan Pilih Produk Kami !'); window.location='produk.php';</script>";
							}
							?>
					</tbody>
				</table>
				
				<div style="float: right; padding-right: 80px"><a class="btn btn-default update" href="proses-transaksi.php">Proses</a><br><br>	<br><br>	<br><br>	
			
			</div>
			</div>
		</div>
	</section> <!--/#cart_items-->


    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php
include 'footer.php';
} else {
    	echo "<script>window.location.href='login.php';</script>";
    }
?>
