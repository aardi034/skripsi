<?php
	include 'header.php';
	include 'config.php';

if(isset($_SESSION['email'])){

?>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">

							<td class="image">Produk</td>
							<td class="price">ID Transaksi</td>
							<td class="description">Jumlah</td>
							<td class="price">Total</td>
							<td class="price">Status</td>
							<td class="price">Bukti</td>
							<td class="">Action</td> 
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
					// 	$session 	=$_SESSION['email'];
					// 	$sql_cart = mysqli_query($con, "SELECT * FROM pembeli WHERE email=$session ") or die (mysqli_error($con));
					//      $get_cart = mysqli_fetch_array($sql_cart);
					//     $id_pembeli	= $get_cart['id_pembeli'];

					// 	 $sql_c = mysqli_query($con,"SELECT * FROM detail_transaksi INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan WHERE id_pembeli=$id_pembeli ") or die (mysqli_error($con));
        		    	
     //        				$data_detail = mysqli_fetch_array($sql_c);
     //        					$id_de=$data_detail['id_detail'];

					// $query = "SELECT * FROM transaksi WHERE id_detail = $id_de";
										
			  //           $sql_buy = mysqli_query($con, $query) or die (mysqli_error($con));
     //    		    	if(mysqli_num_rows($sql_buy) > 0)
     //        				while($data = mysqli_fetch_array($sql_buy)) {
            				

     //                $sql_b = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN pembeli ON detail_transaksi.id_pembeli = pembeli.id_pembeli WHERE id_detail = $id_de ");
     //                $dt   = mysqli_fetch_array($sql_b);
                      
                      
						$session 	=$_SESSION['email'];
						$sql_cart = mysqli_query($con, "SELECT * FROM pembeli WHERE email='$session' ") or die (mysqli_error($con));
					     $get_cart = mysqli_fetch_array($sql_cart);
					    $id_pembeli	= $get_cart['id_pembeli'];

						// $sql_produk = mysqli_query($con, "SELECT * FROM iklan INNER JOIN detail_transaksi ON iklan.id_iklan = detail_transaksi.id_iklan WHERE id_pembeli = '$id_pembeli'   ") or die (mysqli_error($con));
						// $get_produk = mysqli_fetch_array($sql_produk);

					     

						$query = "SELECT * FROM transaksi INNER JOIN detail_transaksi ON transaksi.id_detail = detail_transaksi.id_detail   INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan  WHERE id_pembeli = '$id_pembeli' ORDER BY id_transaksi desc LIMIT 5  ";

						 $sql_c = mysqli_query($con,$query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_c) > 0)
            				while($data = mysqli_fetch_array($sql_c)) {
						
                      ?>
						<tr>
							<td class="cart_product">
								<img src="../images/<?php echo $data['gambar'];?>" border="0" width="150px" height="150px"/>
							</td>
							<td class="cart_price">
								<p><?=$data['id_transaksi']?></p>
								 
							</td> 
							<td class="cart_description">
								<?=$data['jumlah']?></td>
                      					
								<td class="cart_price">
								<p><?=$data['total']?></p>
							</td>
							<td class="cart_price">
								<p><?=$data['status']?></p>
							</td>
							<?php
							$bukti = $data['bukti'];
							if ($bukti==''){?>
								<td class="cart_delete">
								<a class="cart_quantity_delete"href="ulang-bukti.php?id=<?=$data['id_transaksi']?>"><i class="fa fa-check"></i></a>
								</td><?php }
							?>
								<td class="cart_price">
									<img src="../images/bukti/<?php echo $data['bukti'];?>" border="0" width="150px" height="150px"/>
								<p><?=$data['bukti']?></p>
							</td>

							<?php
								$status= $data['status'];
								if ($status=='Proses Kirim') {?>
								<td class="cart_delete">
								<a class="cart_quantity_delete" href="proses_ubah_status.php?id=<?=$data['id_transaksi']?>" ><i class="fa fa-check"></i></a>
								</td>
								<?php }
							?>


							
							<!-- </td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td> -->
							
						</tr>
		<?php
					} else {
						 echo "<script>alert('Riwayat Belanja Anda Masih Kosong, Silahkan Pilih Produk Kami !'); window.location='produk.php';</script>";
							}
							?>
					</tbody>
				</table>
				
				
			</div>
		</div>
	</section> <!--/#cart_items-->

<?php
include 'footer.php';
?>

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php
} else {
  echo"<script>window.location='login.php';</script>";
}
?>