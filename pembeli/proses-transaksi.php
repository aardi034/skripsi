<?php
include 'header.php';
include 'config.php';
?>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<!-- <div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div>/checkout-options-->

	<!-- 		<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div>/register-req-->
			<?php
						$session 	=$_SESSION['email'];
						$sql_cart = mysqli_query($con, "SELECT * FROM pembeli WHERE email='$session' ") or die (mysqli_error($con));
					     $get_cart = mysqli_fetch_array($sql_cart);
					    
						?>
		<form action="proses.php" method="post" >
	<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="bill-to">
						<p>Pembeli</p></div>
						<div>
							<label>Id Pembeli</label>
							<input type="text" name="id" value="<?=$get_cart['id_pembeli']?>" readonly>
							<label>Nama</label>
							<input type="text" name="nama" value="<?=$get_cart['nama']?>" readonly>	
						</div>
					</div>
					<div class="col-sm-9">
						<div class="order-message">
							<p>Shipping Order</p>
							<table class="table table-condensed" border="2">
								<tr>
									<td class="description" border="2">Id</td>
									<td class="description" border="2">Nama</td>
									<td class="price" border="2">Harga</td>
									<td class="price" border="2">Jumlah</td>
									<td class="price" border="2">Total</td>
								</tr>
							
							<?php 
							//Menampilkan barang dari cart
							$id_pembeli	= $get_cart['id_pembeli'] ;
							$query = "SELECT * FROM cart INNER JOIN iklan ON cart.id_iklan = iklan.id_iklan WHERE id_pembeli='$id_pembeli' ";

						 $sql_c = mysqli_query($con,$query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_c) > 0)
            				while($data = mysqli_fetch_array($sql_c)) {?>
            					<tr>
									<td>
									<input type="" name="id_iklan" value="<?=$data['id_iklan']?>" hidden border="2"><?=$data['id_iklan']?></td>
									<td><?=$data['judul']?></td>
									<td><input type="" name="harga" id="harga" value="<?=$data['harga']?>" autocomplete="on" onfocus="mulaiHitung()"  readonly border="2"></td>
									<td><input class="" type="number" id="jumlah" name="jumlah"  value="1" autocomplete="on" size="2" onfocus="mulaiHitung()"  border="2"></td>
									<td><input type="" name="total" id="total" ></td>
								</tr>
            					<?php
            				}
							?>
						</table>

						
						</div>	
					</div>
					<!-- ongkir -->
					

										
				</div>
			</div>
		<div class="shopper-informations">
			<div class="row">
				<br>
						<label hidden>Kota Asal</label><br>
						<input type="text" id="asal" value="169" hidden><br><br>
						<?php
						//Get Data Provinsi
					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "GET",
					  CURLOPT_HTTPHEADER => array(
					    "key: 2f9ebf9e6740b3a410517f8b8f2ade6b"
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					echo "<div class='col-sm-4 col-sm-offset-1'><b>Provinsi Tujuan<br><br>";
					echo "<select name='provinsi' class='form-control' id='provinsi'>";
					echo "<option value=''>Pilih Provinsi Tujuan</option>";
					$data = json_decode($response, true);
					for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
						echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
					}
					echo "</select><br><br>";
					//Get Data Provinsi

				?>
						<label>Kabupaten Tujuan</label><br>
						<select id="kabupaten" name="kabupaten" class="form-control"></select><br><br>

						<label>Kurir</label><br>
						<select id="kurir" name="kurir" class="form-control" >
							<option value="jne">JNE</option>
							<option value="tiki">TIKI</option>
							<option value="pos">POS INDONESIA</option>
						</select><br><br>

						<label>Berat (gram)</label><br>
						<input id="berat" type="text" name="berat" value="500" hidden="" />
						<br><br>

					<!-- 	<input class="btn btn-primary " id="cek" type="submit" value="Cek Ongkir"/><br><br><br> -->
					</div>
					<div class="col-sm-4">
						<div class="form-group">
						
							<label>Ongkos Kirim</label>
							<input class="form-control" type="text" name="ongkir" id="biaya_ongkir">
							<br>
							<input class="btn btn-primary pull-right" name="simpan" type="submit" value="simpan">
						</div>
						
						</div>
			</div>
		</div>
		</form>
			<!-- <div class="review-payment">
				<h2>Review & Payment</h2>
			</div>
			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Shopper Information</p>
							<form>
								<input type="text" placeholder="Display Name">
								<input type="text" placeholder="User Name">
								<input type="password" placeholder="Password">
								<input type="password" placeholder="Confirm password">
							</form>
							<a class="btn btn-primary" href="cart.php">Cancel</a>
							<a class="btn btn-primary" href="">Checkout</a>
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Bill To</p>
							<div class="form-one">
								<form>
									<input type="text" placeholder="Nama">
									<input type="text" placeholder="Email*">
									<input type="text" placeholder="Alamat">
									<select>
										<option>-- Kota --</option>
										<option>Sukoharjo</option>
										<option>Sragen</option>
										<option>Wonogiri</option>
										<option>Klaten</option>
										<option>Surakarta</option>
										<option>Sukoharjo</option>
									</select>
								</form>
								<form>
									<input type="text" placeholder="Ongkos Kirim">
								</form>
							</div> 	
			</div>-->
		
	</section> <!--/#cart_items-->

	

	<?php
		include 'footer.php';
	
	?>
<script type="text/javascript">
function mulaiHitung(){
		Interval = setInterval("hitung()",1);
							}
							function hitung(){
								harga = parseInt(document.getElementById("harga").value);
								jumlah = parseInt(document.getElementById("jumlah").value);
								total = harga * jumlah;
							document.getElementById("total").value = total;
									}
							function berhentiHitung(){
							clearInterval(Interval);
									}

								</script>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#provinsi').val("");

		$('#provinsi').change(function(){

			//Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax 
			var prov = $('#provinsi').val();

      		$.ajax({
            	type : 'GET',
           		url : 'http://localhost/rajaongkir/cek_kabupaten.php',
            	data :  'prov_id=' + prov,
					success: function (data) {
						
					//jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
					$("#kabupaten").html(data);
				}
          	});
		});

		$("#kurir").click(function(){
			//Mengambil value dari option select provinsi asal, kabupaten, kurir, berat kemudian parameternya dikirim menggunakan ajax 
			var asal = $('#asal').val();
			var kab = $('#kabupaten').val();
			var kurir = $('#kurir').val();
			var berat = $('#berat').val();

      		$.ajax({
            	type : 'POST',
           		url : 'http://localhost/rajaongkir/cek_ongkir.php',
            	data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat},
					success: function (data) {

					//console.log(data);
					var hasil = JSON.parse(data);
					var ongkir = hasil.rajaongkir.results[0].costs[0].cost[0].value;
					
					//jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
					//$("#ongkir").text(data);
					$("#biaya_ongkir").val(ongkir);
				}
          	});
		});
	});
</script>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>