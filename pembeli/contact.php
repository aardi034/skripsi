<?php
include 'header.php';
?>
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Contact <strong>Us</strong></h2>    	
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Note</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
	    				<form>
	    					<p>Karya Ini Kami Persembahkan Kepada <b>Kedua Orang Tua</b>,<b>Pembimbing Skripsi</b>, Dan Segenap teman-teman Sistem Informasi angkatan 2015 Fakultas Ilmu Komputer Universitas Duta Bangsa Surakarta</p>
	    					<br>
	    					<p><i><b>Apabila Masih terdapat kekurangan dalam implementasi program kami mohon maaf, Semoga sistem ini dapat bermanfaat dan dapat menjadi referensi bagi penelitian yang akan datang </p>
	    						<br><br><br>
	    					<p align="left"><u> Penulis</p></u>
	    				</form>

	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contact Info</h2>
	    				<address>
	    					<p>Contact Center Administrator</p>
	    					<p>Sampaikan Keluhan Anda</p>
							<p>WA   : +62 83 838 700 173</p>
							<p>Telp : +62 83 838 700 173</p>
							<p>Email: ardyan1501@gmail.com</p>
	    				</address>
	    				<hr>
	    				<address>
	    					<p>Terimakasih Kepada</p>
							<p>Segenap Pengurus Desa Wisata</p>
							<p>Berjo,Ngargoyoso-Karanganyar</p><br>
							<p>Info Developer </p>
							<p>Telp : +62 83 838 700 173</p>
							<p>Email: ardyan1501@gmail.com</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Social Networking</h2>
							<ul>
								<li>
									<a target="_blank" href="http://www.facebook.com/martaardiyanto"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a target="_blank" href="http://www.twitter.com/marta_ardiyanto"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a target="_blank" href="http://www.instagram.com/marta_ardiyanto"><i class="fa fa-instagram"></i></a>
								</li>
							<br>
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
	<?php
	include 'footer.php';
	?>