<?php
include 'header.php';
include 'config.php';
?>

<!-- <?php

	// //Get Data Kabupaten
	// $curl = curl_init();	
	// curl_setopt_array($curl, array(
	//   CURLOPT_URL => "http://api.rajaongkir.com/starter/city?id=169",
	//   CURLOPT_RETURNTRANSFER => true,
	//   CURLOPT_ENCODING => "",
	//   CURLOPT_MAXREDIRS => 10,
	//   CURLOPT_TIMEOUT => 30,
	//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	//   CURLOPT_CUSTOMREQUEST => "GET",
	//   CURLOPT_HTTPHEADER => array(
	//     "key: 2f9ebf9e6740b3a410517f8b8f2ade6b"
	//   ),
	// ));

	// $response = curl_exec($curl);
	// $err = curl_error($curl);

	// curl_close($curl);

	// echo "<label>Kota Asal</label><br>";
	// echo "<select name='asal' id='asalX'>";
	// echo "<option value=''>Pilih Kota Asal</option>";
	// 	$data = json_decode($response, true);
	// 	for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
	// 	    echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
	// 	}
	// echo "</select><br><br><br>";
	// //Get Data Kabupaten


	//-----------------------------------------------------------------------------

	//Get Data Provinsi
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: 2f9ebf9e6740b3a410517f8b8f2ade6b"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	echo "Provinsi Tujuan<br>";
	echo "<select name='provinsi' id='provinsi'>";
	echo "<option value=''>Pilih Provinsi Tujuan</option>";
	$data = json_decode($response, true);
	for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
		echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
	}
	echo "</select><br><br>";
	//Get Data Provinsi

?> -->

<!DOCTYPE html>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Cek Ongkir JNE, TIKI dan Pos Indonesia Menggunakan API RajaOngkir">
    <meta name="author" content="Didin Studio">
	<title>Cek Ongkir JNE, TIKI dan Pos Indonesia Menggunakan API RajaOngkir</title>
	<link rel="icon" href="https://ui.didinstudio.com/img/Icon.png" sizes="16x16" type="image/png">
     
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  </head>
	<body>
		<br>
		<label hidden>Kota Asal</label><br>
		<input type="text" id="asal" value="169" hidden><br><br>
		<?php
		//Get Data Provinsi
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: 2f9ebf9e6740b3a410517f8b8f2ade6b"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	echo "<div class='col-sm-4 col-sm-offset-1'><b>Provinsi Tujuan<br><br>";
	echo "<select name='provinsi' class='form-control' id='provinsi'>";
	echo "<option value=''>Pilih Provinsi Tujuan</option>";
	$data = json_decode($response, true);
	for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
		echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
	}
	echo "</select><br><br>";
	//Get Data Provinsi

?>
		<label>Kabupaten Tujuan</label><br>
		<select id="kabupaten" name="kabupaten" class="form-control"></select><br><br>

		<label>Kurir</label><br>
		<select id="kurir" name="kurir" class="form-control">
			<option value="jne">JNE</option>
			<option value="tiki">TIKI</option>
			<option value="pos">POS INDONESIA</option>
		</select><br><br>

		<label>Berat (gram)</label><br>
		<input id="berat" type="text" name="berat" value="500" />
		<br><br>

		<input class="btn btn-primary " id="cek" type="submit" value="Cek Ongkir"/><br><br><br></div>
<div class="col-sm-4">
		<div class="form-group">
		<form>
			<label>Ongkos Kirim</label>
			<input class="form-control" type="text" name="ongkir" id="biaya_ongkir">
<br>
			<input class="btn btn-primary pull-right" name="simpan" type="submit" value="simpan">
		</form>

</div>
	</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#provinsi').val("");

		$('#provinsi').change(function(){

			//Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax 
			var prov = $('#provinsi').val();

      		$.ajax({
            	type : 'GET',
           		url : 'http://localhost/rajaongkir/cek_kabupaten.php',
            	data :  'prov_id=' + prov,
					success: function (data) {
						
					//jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
					$("#kabupaten").html(data);
				}
          	});
		});

		$("#cek").click(function(){
			//Mengambil value dari option select provinsi asal, kabupaten, kurir, berat kemudian parameternya dikirim menggunakan ajax 
			var asal = $('#asal').val();
			var kab = $('#kabupaten').val();
			var kurir = $('#kurir').val();
			var berat = $('#berat').val();

      		$.ajax({
            	type : 'POST',
           		url : 'http://localhost/rajaongkir/cek_ongkir.php',
            	data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat},
					success: function (data) {

					//console.log(data);
					var hasil = JSON.parse(data);
					var ongkir = hasil.rajaongkir.results[0].costs[0].cost[0].value;
					
					//jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
					//$("#ongkir").text(data);
					$("#biaya_ongkir").val(ongkir);
				}
          	});
		});
	});
</script>

<!-- <?php
// "<div>";
		// include 'footer.php';
	
	?> -->

 <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>