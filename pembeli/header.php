<?php
include 'config.php';
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Electronic Market Ngargoyoso</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	   <?php
session_start();
?>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a target="_blank" href="http://www.whatsapp.com?id=083838700173"><i class="fa fa-phone"></i> +62 83 838 700 173</a></li>
								<li><a target="_blank" href="http://www.gmail.com/mail"><i class="fa fa-envelope"></i> ardyan1501@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a target="_blank" href="http://www.facebook.com/martaardiyanto"><i class="fa fa-facebook"></i></a></li>
								<li><a target="_blank" href="http://www.twitter.com/marta_ardiyanto"><i class="fa fa-twitter"></i></a></li>
								<li><a target="_blank" href="http://www.instagram.com/marta_ardiyanto"><i class="fa fa-instagram"></i></a></li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="index.html"><img src="../images/Untitled-1.png" alt="" /></a>
						</div>
						<!-- <div class="btn-group pull-right clearfix">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div> -->
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								<li><a href="account.php"><i class="fa fa-user"></i> Account</a></li>
								<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								<li><a href="riwayat.php"><i class="fa fa-list"></i> Riwayat</a></li>
								<?php
								if(isset($_SESSION['email'])){
									?>
								<li><a href="logout.php"><i class="fa fa-lock"></i> Logout</a></li>
								<?php
									} else{ 
								?>
								<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>
								<?php
							}
							?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="produk.php" class="active">Produk</a></ li>
								 <li class="dropdown"><a href="#">Kategori<i class="fa fa-angle-down" name="kategori"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li>
										<form action="cari.php" method="get">
										<?php
				$query 	= "SELECT * FROM kategori order by id_kat";
								$sql_k	= mysqli_query($con,$query);
								while($data 	= mysqli_fetch_array($sql_k)):
									?>
								<li><a href="cari.php?cari=<?=$data['id_kat']?>"><?=$data['nama']?></a></li>
								
						<?php
							endwhile;
						?>		
				</form>
                                    </ul> 
								<li><a href="blog.php">Blog</a>
                                </li>  
								<!--  <li><a href="404.html">404</a></li> -->
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</div>

					</div>
					
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->