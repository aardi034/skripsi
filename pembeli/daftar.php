<?php
include 'header.php';
require_once "config.php";

?>
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="form-group"><!--login form-->
						<h3>Register Your Account !</h3>
						<br>
						<form action="proses-pembeli.php" method="post">
						<label for="nama">Nama</label>
							<input class="form-control" type="text" name="nama" id="nama"placeholder="Nama" autofocus/><br>
						<label for="jenkel">Jenis Kelamin</label>
									<select name="jenkel" class="form-control" id="jenkel" name="jenkel">
										<option value="">Jenis Kelamin</option>
										<option value="Laki-laki">Laki-laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
										<br>
						 <label for="email">Email</label>
							<input class="form-control" type="email" name="email" id="email"  placeholder="Email" />
						</div>
						</div>
				<div class="col-sm-4">
					<div class="form-group"><!--sign up form-->
						<h3><br></h3>
						<br>
							<!-- <form action="proses-pembeli.php" method="post"> -->
							<label for="alamat">Alamat</label>
								<input class="form-control" type="text" name="alamat" id="alamat"  placeholder="Alamat" />
								<br>
							<label for="pass">Password</label>
							<input class="form-control" type="password" name="pass" id="pass"placeholder="Password" />

							 <input class="btn btn-primary" name="add" type="submit" value="Simpan">
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
<?php
include 'footer.php';
?>