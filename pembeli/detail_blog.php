<?php
include 'header.php';
?>
<div class="col-sm-9">
					<div class="blog-post-area">
						<?php
						require_once 'config.php';
						//Query data dari database 
						$id 		= $_GET['id'];
						$sql_art	= mysqli_query($con,"SELECT * FROM blog WHERE id = ".$id); 
						$data 	= mysqli_fetch_array($sql_art);					
								?>
								<div class="pager-area">
								<ul class="pager pull-right">
									<li><a href="blog.php" >Back</a></li>
								</ul>
							</div>
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3><?=$data['judul']?></h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Admin</li>
									<li><i class="fa fa-clock-o"></i> <?=$data['id']?></li>
									<li><i class="fa fa-calendar"></i> <?=$data['tanggal']?></li>
								</ul>
								<span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<a href="">
								<img src="../images/<?php echo $data['gambar'];?>"  height="500">
							</a>
							<p><?php echo $data['isi']?></p>
							<div class="pager-area">
								<ul class="pager pull-right">
									<li><a href="#">Pre</a></li>
									<li><a href="#">Next</a></li>
								</ul>
							</div>
						</div>
					</div>
					<?php
					include 'footer.php';
					?>