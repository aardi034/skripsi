<?php
include 'header.php';
include 'config.php';
?>

<body>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->
<?php
						$session 	=$_SESSION['user'];
						$sql_cart = mysqli_query($con, "SELECT * FROM pembeli WHERE username='$session' ") or die (mysqli_error($con));
					     $get_cart = mysqli_fetch_array($sql_cart);
					     ?>
			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-6">
						<div class="bill-to">
						<p>Data barang</p>
							<table class="table table-condensed">
								<tr>
									<td class="description">Id</td>
									<td class="description">barang</td>
									<td class="price">harga</td>
								</tr>
							
							<?php 
							//Menampilkan barang dari cart
							$id_pembeli	= $get_cart['id_pembeli'];
							$query = "SELECT * FROM cart INNER JOIN iklan ON cart.id_iklan = iklan.id_iklan WHERE id_pembeli='$id_pembeli' ";

						 $sql_c = mysqli_query($con,$query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_c) > 0)
            				while($data = mysqli_fetch_array($sql_c)) {?>
            					<tr>
									<td><?=$data['id_iklan']?></td>
									<td><?=$data['judul']?></td>
									<td><?=$data['harga']?></td>
								</tr>
            					<?php
            				}
							?>
						</table>
						<div>
							<table>
								<tr>
									<td>Jumlah</td>
									<td>:</td>
									<td>Count data cart</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<table class="table table-condensed">
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2"></td>
								<table class="table table-condensed total-result">
									<tr>
								<td><h6>Status</h6></td>
								<td>:</td>
								<td><?php
									$ket=$get_status['keterangan'];
									if ($ket=='') {
										echo "Pemesanan anda Masih diproses";
									} else {
										echo $ket;
									}
								?></td>
									
							</tr>
								</table>
						
						</div>	
					</div>					
				</div>
			</div>
			
			</div>
			
		</div>
	</section> <!--/#cart_items-->

	

	<?php
		include 'footer.php';
	?>


    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>