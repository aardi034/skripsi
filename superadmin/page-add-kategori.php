<?php
include 'header.php';
include 'nav.php';

 if(isset($_SESSION['superadmin'])) {

?>
<main class="app-content">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-th-list"></i> Tambah Kategori</h1>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
                <form action="proses-kategori.php" method="post">
                  <div class="form-group">
                    <label for="nama">Nama Kategori</label>
                    <input class="form-control" id="nama" name="nama" type="text"  placeholder="Masukkan Kategori Baru"></div>
						<div class="tile-footer">
							<input class="btn btn-primary" name="add" type="submit" value="Simpan">
						</div>
				</form>
			  </div>
			 </div>
		  </div>
		</div>
	</div>
</div>
</main>
       <?php

include 'footer.php';
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>