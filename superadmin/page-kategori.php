<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

 if(isset($_SESSION['superadmin'])) {

?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Kategori</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="pull-right">   
                        <a href="page-add-kategori.php" class="btn pull-right btn-primary"><i class="fa fa-plus"></i> Tambah Kategori</a>
            </div><br><br>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
            	    	<th>Nomor</th>
            	    	<th>Nama Kategori</th>
            	    	<th><i class="fa fa-cog"></i></th>
            		</tr>
        		</thead>
        	<tbody>
        	<?php
        	$nomor = 1;
        	//Query Data Dari DataBase
					$query = "SELECT * FROM kategori";

			            $sql_k = mysqli_query($con,$query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_k) > 0)
            				while($data = mysqli_fetch_array($sql_k)) {?>
            				
                  <tr>
            					<td><?=$nomor++?></td>
            					<td><?=$data['nama']?></td>
            					<td class="text-center">
										<a href="page-edit-kat.php?id=<?=$data['id_kat']?>" class ="btn btn-xs btn-warning btn-xs"><i  class="fa fa-pencil"></i>Edit</a>
											
										<a href="page-del-kat.php?id=<?=$data['id_kat']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini? ')" class ="btn btn-xs btn-danger " id="demoSwal"><i  class="fa fa-trash"></i>Hapus</a>
								</td>
            				</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
        <?php

include 'footer.php';
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>

    	 <script type="text/javascript" src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
    <script type="text/javascript">
      $('#demoNotify').click(function(){
        $.notify({
          title: "Update Complete : ",
          message: "Something cool is just updated!",
          icon: 'fa fa-check' 
        },{
          type: "info"
        });
      });
      $('#demoSwal').click(function(){
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
        }, function(isConfirm) {
          if (isConfirm) {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
          } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      });
    </script>
