<?php
include 'header.php';
include 'nav.php';

 if(isset($_SESSION['superadmin'])) {

?>
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Tambah Member</h1>
    </div>
  </div>
  <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
                <form action="proses-jual.php" method="post"  >
                  <div class="form-group">
                    <label for="nama">Nama Member</label>
                      <input class="form-control" id="nama" name="nama" type="text"  placeholder="Masukkan Nama Member Baru"></div>
                   <div class="form-group">
                    <label for="alamat">Alamat</label>
                      <input class="form-control" id="alamat" name="alamat" type="text"  placeholder="Masukkan Alamat Member"></div>
                        <div class="form-group">
                    <label for="email">Alamat Email</label>
                      <input class="form-control" id="email" name="email" type="text"></div>
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                      <input class="form-control" id="telepon" name="telepon" type="text"></div>
                  </div>
              <div class="col-lg-4 offset-lg-1">
                  <div class="form-group">
                    <label for="username">Username</label>
                      <input class="form-control" id="username" name="username" type="text"></div>
                  <div class="form-group">
                    <label for="password">Password</label>
                      <input class="form-control" id="password" name="password" type="password"></div>
                <form>
                  <div class="form-group">
            <div class="tile-footer">
              <input class="btn btn-primary" name="add" type="submit" value="Simpan">

              <button class="btn btn-warning" name="reset" type="reset" >Reset</button>
            </div>
  </div>          
        </form>
        </div>
       </div>
      </div>
    </div>
  </div>
</div>
</main>
       <?php

include 'footer.php';
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>