<?php
include 'header.php';
include 'nav.php';
include 'footer.php';
include 'config/config.php';

?>
<main class="app-content">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-th-list"></i> Edit Member</h1>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
              	<?php
					 $id = $_GET['id'];
					 $sql_sel = mysqli_query($con, " SELECT * FROM penjual WHERE id_penjual ='$id' ") or die (mysqli_error($con));
					 $data = mysqli_fetch_array($sql_sel);
					?>
                <form action="proses-edit-jual.php" method="post">
                  <div class="form-group">
                  	<label for="id">ID</label>
                  	<input class="form-control" id="id" name="id" type="text" value="<?php echo $id?>" readonly>
                   <label for="nama">Nama Member</label>
                      <input class="form-control" id="nama" name="nama" type="text" value="<?php echo $data['nama']?> "></div>
                   <div class="form-group">
                    <label for="alamat">Alamat</label>
                      <input class="form-control" id="alamat" name="alamat" type="text"  value="<?php echo $data['alamat']?> "></div>
                       
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                      <input class="form-control" id="telepon" name="telepon" type="text" value="<?php echo $data['telepon']?> "></div>
                  </div>
              <div class="col-lg-4 offset-lg-1">
                  <div class="form-group">
                    <label for="username">Username</label>
                      <input class="form-control" id="username" name="username" type="text" value="<?php echo $data['username']?> "></div>
                  <div class="form-group">
                    <label for="password">Password</label>
                      <input class="form-control" id="password" name="password" type="text" value="<?php echo $data['password']?> "></div>
						<div class="tile-footer">
							<input class="btn btn-primary" name="edit" type="submit" value="Simpan">
						</div>
				</form>
			  </div>
			 </div>
		  </div>
		</div>
	</div>
</div>
</main>