<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

 if(isset($_SESSION['superadmin'])) {
?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="icon fa fa-users"></i> Transaksi</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Pembeli</th>
                    <th>Alamat</th>
                    <th>Barang</th>
                    <th>Penjual</th>
                    <!-- <th>Harga</th> -->
                    <th>Jumlah</th>
                    <!-- <th>Ongkir</th> -->
                    <th>Total</th>
                    <th>Status</th>
                    <th>Bukti</th>
                    <th><i class="fa fa-cog"></i></th>
                </tr>
            </thead>
          <tbody>
           <?php
         //  $nomor = 1;
          //Query Data Dari DataBase
          $query = "SELECT * FROM transaksi ";
                    
                  $sql_buy = mysqli_query($con, $query) or die (mysqli_error($con));
                  if(mysqli_num_rows($sql_buy) > 0)
                    while($data = mysqli_fetch_array($sql_buy)) {
                    $id_detail = $data['id_detail'];
                    $sql_b = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN pembeli ON detail_transaksi.id_pembeli = pembeli.id_pembeli WHERE id_detail='$id_detail' ");
                    $dt   = mysqli_fetch_array($sql_b);
                    ?>
                    <tr>
                      <td><?=$data['id_transaksi']?></td>
                      <td><?=$dt['nama']?></td>
                      <td><?=$dt['alamt']?></td>
                      <?php
                        $sql_c = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan WHERE id_detail='$id_detail' ");
                        $d   = mysqli_fetch_array($sql_c);
                      ?>
                      <td><?=$d['judul']?></td>
                      <td><?php $id_penjual=$d['id_penjual'];
                      $sql_detail = mysqli_query($con, "SELECT * FROM penjual WHERE id_penjual='$id_penjual' ") or die (mysql_error($con));
                      $get_detail = mysqli_fetch_array($sql_detail);
                      echo $get_detail['nama'];?></td>
                      <!-- <td><?=$d['harga']?></td> -->
                      <td><?=$data['jumlah']?></td>
                      <!-- <td><?=$data['ongkir']?></td> -->
                      <td><?=$data['total']?></td>
                      <td><?=$data['status']?></td> 
                      <td><img src="../images/bukti/<?php echo $data['bukti'];?>" border="0" width="80px" height="80px"/></td>
                      <td><a href="laporan/invoice.php?id=<?=$data['id_transaksi']?>" target="_blank" class ="btn btn-xs btn-warning btn-xs"><i  class="fa fa-print"></i>Cetak</a>
                    
                </td>
                    </tr>
                    <?php
          } else {
            echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
              }
              ?>
           </tbody>
        </table>
 <?php
      include 'footer.php';
      } else {
  echo"<script>window.location.href='login.php';</script>";
}
      ?>
         <script type="text/javascript" src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
      }
    </script>