<?php
include 'header.php';
include 'nav.php';
include 'footer.php';

?>
<main class="app-content">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-th-list"></i> Edit Kategori</h1>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
              	<?php
				require 'config/config.php';
				
					 $id = $_GET['id'];
					 $sql_k = mysqli_query($con, " SELECT * FROM kategori WHERE id_kat ='$id' ") or die (mysqli_error($con));
					 $data = mysqli_fetch_array($sql_k);
					?>
                <form action="kategori-edit-proses.php" method="post">
                  <div class="form-group">
                  	<label for="id">ID</label>
                  	<input class="form-control" id="id" name="id" type="text" value="<?php echo $id?>" readonly>
                    <label for="nama">Nama Kategori</label>
                    <input class="form-control" id="nama" name="nama" type="text"  placeholder="Masukkan Kategori Baru"></div>
						<div class="tile-footer">
							<button class="btn btn-primary" name="edit" type="submit">Simpan</button>
						</div>
				</form>
			  </div>
			 </div>
		  </div>
		</div>
	</div>
</div>
</main>