<?php
  
  include 'header.php';
  include 'nav.php';
  include 'config/config.php';


 if(isset($_SESSION['superadmin'])) {
?>

<div class="app-content"> 
  <div class="app-title">
              <h5>Selamat Datang <mark><?=$_SESSION['superadmin'];?></mark> di Website E-Market Ngargoyoso </p></h5>
  </div>
 <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
              <h5>Penjual</h5>
              <p><b><?php
                $sql_jual = "SELECT * FROM penjual";
        $query = mysqli_query($con,$sql_jual);
        $data = array();
        while(($row = mysqli_fetch_array($query)) != null){
          $data[] = $row;
        }
        $count = count($data);
        echo "$count"; ?> </b></p>
            </div>
          </div>
        </div>
         <div class="col-md-6 col-lg-3">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-film fa-3x"></i>
            <div class="info">
              <h5>Iklan</h5>
              <p><b><?php
                $sql_ik = "SELECT * FROM iklan";
        $query = mysqli_query($con,$sql_ik);
        $data = array();
        while(($row = mysqli_fetch_array($query)) != null){
          $data[] = $row;
        }
        $count = count($data);
        echo "$count"; ?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-eye fa-3x"></i>
            <div class="info">
              <h5>Pengunjung</h5>
              <p><b><?php
                $sql_p = "SELECT * FROM pembeli";
        $query = mysqli_query($con,$sql_p);
        $data = array();
        while(($row = mysqli_fetch_array($query)) != null){
          $data[] = $row;
        }
        $count = count($data);
        echo "$count"; ?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-money fa-3x"></i>
            <div class="info">
              <h5>Profit</h5>
              <p><b>Rp.  <?php
                $sql_profit = mysqli_query($con,"SELECT SUM(total) as total FROM transaksi WHERE status='Sudah Diterima' ") or die (mysqli_error($con));
                $data = mysqli_fetch_array($sql_profit);
        echo $data['total'] ; ?> </b></p>
            </div>
          </div>
        </div>
      </div><br>
<!-- 
      <div class="app-title">
        <div>
          <h1><i class="fa fa-pie-chart"></i> Product Cart Monitoring <i><b>Ngargoyoso Marketplace</b></i></h1>
          <p>Produk Terlaris</p>
        </div>
              </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Line Chart</h3>
              
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Bar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Radar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="radarChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Polar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="polarChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Pie Chart</h3>
    
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Doughnut Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="doughnutChartDemo"></canvas>
            </div>
          </div>
        </div> -->
      </div>
    </main>
    
      <!-- <div class="col-lg-12">
          <h1 align="center">SISTEM INFORMASI REKAM MEDIS</h1>
          <p align="center">Selamat Datang<mark><?=$_SESSION['user'];?></mark> di Website Rekam Medis </p>
         
      </div>
             <?php
            // Query Data
            // $query = "SELECT detail_transaksi.id_iklan,iklan.id_iklan,transaksi.id AND WHERE id_detail='$id_detail' ";
                    
                  // $sql_buy = mysqli_query($con, $query) or die (mysqli_error($con));
                  // if(mysqli_num_rows($sql_buy) > 0)
                    // $data = mysqli_fetch_array($sql_buy) 
                    // $id_detail = $data['id_detail'];
                    // $sql_b = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan WHERE id_detail='$id_detail' ");
                    // $dt   = mysqli_fetch_array($sql_b);
                      ?> 

       -->
        <!--  -->
<!-- <div class="app-content"> -->
  <!-- <div class="row page-titles">
   --><!--  <div class="col-md-6 col-8 align-self-center">
      <h3>Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </div> 
  </div>
  <div class="row">  
    <div class="col-12">
        <div class="p-20">
          <h2>Welcome to Administrator</h2>
          <h6>Silahkan pilih menu di sebelah kiri untuk mengoprasikannya</h6> 
        </div> 
      </div>
    </div> 
  </div>   -->



<!-- 
 <?php
mysql_connect('localhost', 'username', 'password');
mysql_select_db('database_name');
 
$table = "table_name";
 
// Cara 1
$sql = "SELECT count(*) AS jumlah FROM $table";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
echo "Jumlah data dengan fungsi MySQL count(): {$result['jumlah']} <br/>";

// Cara 2
$sql = "SELECT * FROM $table";
$query = mysql_query($sql);
$count = mysql_num_rows($query);
echo "Jumlah data dengan mysql_num_rows: $count <br/>";
 
// Cara 3
$sql = "SELECT * FROM $table";
$query = mysql_query($sql);
$data = array();
while(($row = mysql_fetch_array($query)) != null){
    $data[] = $row;
}
$count = count($data);
echo "Jumlah data dari array PHP: $count";
?> -->

<?php
include 'footer.php';
 
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>