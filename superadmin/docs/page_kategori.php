<?php
include 'header.php';
include 'nav.php';
include 'footer.php';

?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Kategori</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
            	    	<th>ID Kategori</th>
            	    	<th>Nama Kategori</th>
            	    	<th><i class="fa fa-cog"></i></th>
            		</tr>
        		</thead>
        	<tbody>
        	<?php
include '../config/config.php';
        	?>
        	<?php
        	//Query Data Dari DataBase
					$query = "SELECT * FROM kategori";
										
			            $sql_k = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_k) > 0)
            				while($data = mysqli_fetch_array($sql_k)) {?>
            				

            				<tr>
            					<td><?=$data['id_kat']?></td>
            					<td><?=$data['nama_kat']?></td>
            					<td class="text-center">
										<a href="edit_kategori.php?id=<?=$data['id_kat']?>" class ="btn btn-warning btn-xs"><i  class="fa fa-pencil"></i></a>
											
										<a href="del_kategori.php?id=<?=$data['id_kat']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini? ')" class ="btn btn-danger btn-xs"><i  class="fa fa-trash"></i></a>
								</td>
            				</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
    		 <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>