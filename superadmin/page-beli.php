<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

 if(isset($_SESSION['superadmin'])) {
?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="icon fa fa-users"></i> Daftar Pembeli</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Password</th>
                    <th><i class="fa fa-cog"></i></th>
            		</tr>
        		</thead>
        	<tbody>
        	 <?php
         //  $nomor = 1;
        	//Query Data Dari DataBase
					$query = "SELECT * FROM pembeli";
										
			            $sql_buy = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_buy) > 0)
            				while($data = mysqli_fetch_array($sql_buy)) {?>
            				<tr>
            					<td><?=$data['id_pembeli']?></td>
                      <td><?=$data['nama']?></td>
                      <td><?=$data['jenis_kelamin']?></td>
                      <td><?=$data['email']?></td>
                      <td><?=$data['alamt']?></td>
                      <td><?=$data['password']?></td>
										<td><a href="page-del-beli.php?id=<?=$data['id_pembeli']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini? ')" class ="btn btn-xs btn-danger "><i  class="fa fa-trash"></i>Hapus</a>
								</td>
            				</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
 <?php
include 'footer.php';
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>
    		 <script type="text/javascript" src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>