<?php
include 'header.php';
include 'nav.php';


 if(isset($_SESSION['superadmin'])) {

?>
<main class="app-content">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-th-list"></i> Tambah Artikel</h1>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
                <form action="proses-blog.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="judul">Judul Artikel</label>
        	            <input class="form-control" id="judul" name="judul" type="text"  placeholder="Masukkan Judul Baru"></div>
                  </div>
                   </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control" id="isi" name="isi" rows="3" placeholder="Masukkan Isi Artikel Anda"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="tanggal">Tanggal</label>
                      <input class="form-control" id="tanggal" name="tanggal" type="date"></div>

                  <div class="form-group">
                    <label for="gambar">Gambar</label>
                    <input class="form-control-file" id="gambar" type="file" name="gambar"><small class="form-text text-muted">Masukkan File Gambar Dari Artikel Anda Maks 2 MB</small>
                  </div>
                  
                <form>
                  <div class="form-group">
						<div class="tile-footer">
							<button class="btn btn-primary" name="add" type="submit" value="upload">Simpan</button>
						</div>
				</form>
			  </div>
			 </div>
		  </div>
		</div>
	</div>
</div>
</main>
       <?php

include 'footer.php';
} else {
  echo"<script>window.location.href='login.php';</script>";
}
?>