-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2019 at 07:36 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` varchar(250) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `isi`, `gambar`, `tanggal`) VALUES
(5, 'Festival Payung', 'Pameran Payung Terbesar Di Solo Raya', '11-300x300.jpg', '2019-12-03'),
(6, 'Festival Duren', 'Dalam Rangka HUT Karanganyar Ke - 200', '7.jpg', '2019-11-09');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(10) NOT NULL,
  `id_pembeli` int(10) NOT NULL,
  `id_iklan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_detail` int(10) NOT NULL,
  `id_iklan` int(10) NOT NULL,
  `id_pembeli` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_detail`, `id_iklan`, `id_pembeli`) VALUES
(31, 17, 2),
(32, 18, 2),
(33, 16, 2),
(35, 16, 2),
(36, 16, 2),
(37, 13, 2),
(38, 7, 1),
(39, 19, 1),
(40, 19, 1),
(41, 12, 1),
(42, 16, 1),
(43, 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE `iklan` (
  `id_iklan` int(10) NOT NULL,
  `id_penjual` int(2) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `id_kat` int(10) NOT NULL,
  `harga` int(10) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklan`
--

INSERT INTO `iklan` (`id_iklan`, `id_penjual`, `judul`, `id_kat`, `harga`, `gambar`, `deskripsi`) VALUES
(7, 2, 'keripik gethuk', 1, 8000, 'img-1561953475.jpg', 'Halal'),
(8, 2, 'keipik pisang', 1, 8000, 'img-1561953522.jpg', 'halal'),
(9, 2, 'keripik mint', 1, 9000, 'img-1561953544.jpg', 'keipik pilihan'),
(10, 2, 'keripik ketela', 1, 8000, 'img-1561953579.jpg', 'kerpik ketela ungu\r\n'),
(11, 2, 'keripik bayem', 1, 8000, 'img-1561953630.jpg', 'bayem asli'),
(12, 4, 'Mawar Pink', 3, 20000, 'img-1561953676.jpg', 'Mawar Subur'),
(13, 4, 'Mawar Putih', 3, 30000, 'img-1561953705.jpg', 'Mawar Putih'),
(14, 4, 'Bunga Ungu', 3, 25000, 'img-1561953759.jpg', 'Bunga Ungu'),
(15, 3, 'Gantungan Kunci', 2, 10000, 'img-1561953874.jpg', '10000'),
(16, 3, 'Gantungan Botol', 2, 5000, 'img-1561953901.jpg', 'Gantungan Botol'),
(17, 3, 'Gelang Anti Air', 2, 8000, 'img-1561953965.jpg', 'gelang laki'),
(18, 3, 'Gantungan Centong', 2, 20000, 'img-1561953992.jpg', 'Gantungan'),
(19, 2, 'Gethuk Semar', 1, 20000, 'img-1561954366.jpg', 'Gethuk Enak');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kat` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kat`, `nama`) VALUES
(2, 'Handicraft'),
(1, 'Makanan'),
(3, 'Tanaman'),
(4, 'Teh');

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id_pembeli` int(10) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan','','') NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamt` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id_pembeli`, `nama`, `jenis_kelamin`, `email`, `alamt`, `password`) VALUES
(1, 'marta', 'Laki-laki', 'ardyan14@gmail.com', 'jogja', 'd575507395f399a23f9b995a1e214a669454f99b'),
(2, 'Ardiyan', 'Laki-laki', 'aardi002@gmail.com', 'Surabaya', '1da2352347bdc7c0c7078bd505b96c60c820a61c'),
(3, 'Sarah', 'Perempuan', 'sarah1@gmail.com', 'Palangka Raya', '9447c4dbc86c3bb33129986f9ad1a669bfd7e8ea'),
(4, 'bert', 'Laki-laki', 'bertimeusadhi@gmail.com', 'www', '40bd001563085fc35165329ea1ff5c5ecbdbbeef'),
(5, 'Gethuk Pelangi', 'Laki-laki', 'gethuk1@gmail.com', 'Ngargoyoso', 'f0de4e98d474c0d205349e6d24f1a86b6f4f0ed7');

-- --------------------------------------------------------

--
-- Table structure for table `penjual`
--

CREATE TABLE `penjual` (
  `id_penjual` int(2) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjual`
--

INSERT INTO `penjual` (`id_penjual`, `nama`, `alamat`, `telepon`, `username`, `password`) VALUES
(2, 'Kedaiku', 'Ngargoyoso', '08767766779', 'Kedaiku', 'd9b701105e8ed4677ce0dc2c677c21935a7641c1'),
(3, 'Warmindo', 'Ngargoyoso', '8977788', 'warmindo', 'd24f93dbf016e4a9ff0e7b287e3da05e49affc67'),
(4, 'Savana', 'Berjo Wetan', '0876776677', 'savana1', 'a932f3c7b2f88b577a2717401d9768c32c430297'),
(5, 'Marta', 'Ngargoyoso', '089128776554', 'marta', '5ee498f15e19f53cc1723928c3c42c17d524aaf2'),
(6, 'Ardi', 'Ngargoyoso -jateng', '08767766774', 'masku', '5ee498f15e19f53cc172'),
(7, 'aardi', 'Ngargoyoso', '0876776674', 'marta', '889a3a791b3875cfae41'),
(8, 'Kedai Pak Ahong', 'Ngargoyoso', '08765567667', 'ahong123', '366b6605d06e9b60a324f4d38e1a7a6c90157e6e');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(10) NOT NULL,
  `id_detail` int(10) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `ongkir` int(10) NOT NULL,
  `total` int(10) NOT NULL,
  `status` enum('Belum Diproses','Proses Kirim','Sudah Diterima','') NOT NULL,
  `bukti` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_detail`, `jumlah`, `ongkir`, `total`, `status`, `bukti`) VALUES
(8, 31, 2, 39000, 55000, 'Belum Diproses', 'bukti-1561955639.jpg'),
(9, 32, 2, 9000, 49000, 'Belum Diproses', ''),
(10, 33, 2, 7000, 17000, 'Belum Diproses', ''),
(11, 35, 1, 6000, 11000, 'Belum Diproses', ''),
(12, 36, 1, 7000, 12000, 'Belum Diproses', ''),
(13, 37, 1, 50000, 80000, 'Belum Diproses', ''),
(14, 38, 2, 14000, 30000, 'Sudah Diterima', 'bukti-1562207600.jpg'),
(15, 39, 1, 27000, 47000, 'Sudah Diterima', 'bukti-1562392960.jpg'),
(16, 40, 1, 13000, 33000, 'Sudah Diterima', 'bukti-1562392969.jpg'),
(17, 41, 1, 14000, 34000, 'Belum Diproses', 'bukti-1562392981.jpg'),
(18, 42, 100, 26000, 526000, 'Belum Diproses', 'bukti-1562393022.jpg'),
(19, 43, 12, 54000, 114000, 'Belum Diproses', 'bukti-1562394884.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(2) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `superadmin` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `superadmin`, `password`) VALUES
(2, 'super Admin', 'admin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `id_pembeli` (`id_pembeli`),
  ADD KEY `id_iklan` (`id_iklan`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `id_pembeli` (`id_pembeli`),
  ADD KEY `id_iklan` (`id_iklan`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id_iklan`),
  ADD KEY `id_penjual` (`id_penjual`),
  ADD KEY `id_kat` (`id_kat`),
  ADD KEY `id_kat_2` (`id_kat`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kat`),
  ADD UNIQUE KEY `kategori` (`nama`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indexes for table `penjual`
--
ALTER TABLE `penjual`
  ADD PRIMARY KEY (`id_penjual`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_detail` (`id_detail`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id_detail` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id_iklan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id_pembeli` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `penjual`
--
ALTER TABLE `penjual`
  MODIFY `id_penjual` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id_pembeli`);

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id_pembeli`),
  ADD CONSTRAINT `detail_transaksi_ibfk_4` FOREIGN KEY (`id_iklan`) REFERENCES `iklan` (`id_iklan`);

--
-- Constraints for table `iklan`
--
ALTER TABLE `iklan`
  ADD CONSTRAINT `iklan_ibfk_1` FOREIGN KEY (`id_penjual`) REFERENCES `penjual` (`id_penjual`),
  ADD CONSTRAINT `iklan_ibfk_2` FOREIGN KEY (`id_kat`) REFERENCES `kategori` (`id_kat`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_detail`) REFERENCES `detail_transaksi` (`id_detail`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
