<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

if(isset($_SESSION['username'])){
?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Iklan</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="pull-right">   
                        <a href="page-add-iklan.php" class="btn pull-right btn-primary"><i class="fa fa-plus"></i> Tambah Iklan</a>
            </div><br><br>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
                    <th>ID Iklan</th>
                    <th>Judul Iklan</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Deskripsi</th>
                    <th><i class="fa fa-cog"></i></th>
            		</tr>
        		</thead>
        	<tbody>
        	<?php
          $username = $_SESSION['username'];
                  $sql_kategori = mysqli_query($con," SELECT * FROM penjual WHERE username='$username' "); 
                  $kategori   = mysqli_fetch_array($sql_kategori);
                  $id = $kategori ['id_penjual'];

          //Query Data Dari DataBase
          $query = "SELECT * FROM iklan WHERE id_penjual='$id' ";
										
			            $sql_ik = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_ik) > 0)
            				while($data = mysqli_fetch_array($sql_ik)) { 
                      ?>

            				<tr>
            					<td><?=$data['id_iklan']?></td>
                      <td><?=$data['judul']?></td>
                      <td>
                           <?php
                      $id     = $data['id_kat'];
                  $sql_kategori = mysqli_query($con," SELECT * FROM kategori WHERE id_kat='$id' "); 
                  $kategori   = mysqli_fetch_array($sql_kategori);
                  echo $kategori['nama'];
                ?>
                      </td>
                      <td><?=$data['harga']?></td>
                      <td><img src="../images/<?php echo $data['gambar'];?>" border="0" width="80px" height="80px"/></td>
                      <td><?=$data['deskripsi']?></td>
                      <td class="text-center">
										<a href="page-edit-iklan.php?id=<?=$data['id_iklan']?>" class ="btn btn-xs btn-warning btn-xs"><i  class="fa fa-pencil"></i>Edit</a>
											
										<a href="page-del-iklan.php?id=<?=$data['id_iklan']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini? ')" class ="btn btn-xs btn-danger "><i  class="fa fa-trash"></i>Hapus</a>
								</td>
            				</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
        <?php
include 'footer.php';
} else {
  echo"<script>window.location='login.php';</script>";
}
?>
    		 <script type="text/javascript" src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>