<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

if(isset($_SESSION['username'])){
?>


<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="icon fa fa-users"></i> Transaksi</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">

              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
                    <th>ID</th>
                    <th>Pembeli</th>
                    <th>Alamat</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                    <th>Status</th>
                   
            		</tr>
        		</thead>
        	<tbody>
              <?php
         $username = $_SESSION['username'];
                  $sql_kategori = mysqli_query($con," SELECT * FROM penjual WHERE username='$username' "); 
                  $kategori   = mysqli_fetch_array($sql_kategori);
                  $id = $kategori ['id_penjual'];
                  

          // $nomor = 1; 
                        $sql_f = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan WHERE id_penjual= $id   ");
                        $e    = mysqli_fetch_array($sql_f);
                       $id_de = $e['id_detail'];

        	//Query Data Dari DataBase
					$query = "SELECT * FROM transaksi WHERE id_detail=$id_de";
										
			            $sql_buy = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_buy) > 0)
            				while($data = mysqli_fetch_array($sql_buy)) {
            				$id_detail = $data['id_detail'];
                    $sql_b = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN pembeli ON detail_transaksi.id_pembeli = pembeli.id_pembeli WHERE id_detail='$id_detail' ");
                    $dt   = mysqli_fetch_array($sql_b);
                    ?>
                    <tr>
            					<td><?=$data['id_transaksi']?></td>
                      <td><?=$dt['nama']?></td>
                      <td><?=$dt['alamat']?></td>
                      <?php
                        $sql_c = mysqli_query($con, "SELECT * FROM detail_transaksi INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan WHERE id_detail='$id_detail' ");
                        $d   = mysqli_fetch_array($sql_c);
                      ?>
                      <td><?=$d['judul']?></td>
                    
                      <td><?=$data['jumlah']?></td>
                      <td><?=$data['total']?></td>
                      <td><?=$data['status']?></td> 
                      
								</td>
            				</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
        <?php

include 'footer.php';
} else {
  echo"<script>window.location='login.php';</script>";
}
?>
 <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
      }
    </script>

<script src="assets/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>

<script src="assets/DataTables/Buttons-1.5.4/js/dataTables.buttons.min.js"></script>

<script src="assets/DataTables/Buttons-1.5.4/js/buttons.flash.min.js"></script>

<script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js"></script>

<script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js"></script>

<script src="assets/DataTables/Buttons-1.5.4/js/buttons.html5.min.js"></script>

<script src="assets/DataTables/Buttons-1.5.4/js/buttons.print.min.js"></script>
 <script>
 $(document).ready(function() {
    $('#sampleTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'pdf', 'print'
        ]
    } );
} );
 </script>
