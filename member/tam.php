<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';

?>
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Mitra Usaha</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>
   <div class="pull-right">   
                        <a href="page-add-kategori.php" class="btn pull-right btn-primary"><i class="fa fa-plus"></i> Tambah Member</a>
            </div><br><br>
      <div class="row">
        <div class="col-md-13">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
                    <th>ID</th>
                    <th>Kota</th>
                    <th>tarif</th>
            		</tr>
        		</thead>
        	<tbody>
        	
        	<?php
        	//Query Data Dari DataBase
					$query = "SELECT * FROM ongkir";
										
			            $sql_o = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_o) > 0)
            				while($data = mysqli_fetch_array($sql_o)) {?>
            				<tr>
            		      <td><?=$data['id']?></td>
                      <td><?=$data['kota']?></td>
                      <td><?=$data['tarif']?></td>
                      
								</td>
							</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
    <?php
include 'footer.php';
?>
<script type="text/javascript" src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
