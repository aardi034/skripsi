<?php
include 'header.php';
include 'nav.php';
include 'config/config.php';


if(isset($_SESSION['username'])){
?>
 <link rel="stylesheet" href="assets/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css">

 <link rel="stylesheet" href="assets/DataTables/Buttons-1.5.4/css/buttons.dataTables.min.css">
<div class="app-content">
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Data Mitra Usaha</h1>
        </div>
         <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
        </ul>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
            		<tr>
                    <th>ID</th>
                    <th>Tanggal Daftar</th>
                    <th>Nama</th>
                    <th>Pemilik</th> 
                    <th>Jenis Kelamin</th>
                    <th>TTL</th>
                    <th>Jenis</th>
                    <th>Telepon</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th><i class="fa fa-cog"></i> Aksi</th>
            		</tr>
        		</thead>
        	<tbody>
        	
        	<?php
        	//Query Data Dari DataBase
					$query = "SELECT * FROM mitra";
										
			            $sql_m = mysqli_query($con, $query) or die (mysqli_error($con));
        		    	if(mysqli_num_rows($sql_m) > 0)
            				while($data = mysqli_fetch_array($sql_m)) {?>
            				<tr>
            		      <td><?=$data['id']?></td>
                      <td><?=$data['tanggal']?></td>
                      <td><?=$data['nama']?></td>
                      <td><?=$data['pemilik']?></td>
                      <td><?=$data['jenkel']?></td>
                      <td><?=$data['ttl']?></td>
                      <td><?=$data['jenis']?></td>
                      <td><?=$data['telp']?></td>
                      <td><?=$data['username']?></td>
                      <td><?=$data['password']?></td>
                      <td><a href="page-edit-kat.php?id=<?=$data['id']?>" class ="btn btn-xs btn-warning btn-xs"><i  class="fa fa-pencil"></i>Edit</a>
											
										<a href="page-del-kat.php?id=<?=$data['id']?>" onclick="return confirm('Yakin Akan Menghapus Data Ini? ')" class ="btn btn-xs btn-danger "><i  class="fa fa-trash"></i>Hapus</a>
								</td>
							</tr>
            				<?php
					} else {
						echo "<tr><td colspan=\"9\" align=\"center\">Data Tidak Ditemukan</td></tr>";
							}
							?>
		       </tbody>
    		</table>
    <?php
include 'footer.php';
} else {
  echo"<script>window.location='login.php';</script>";
}

?>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
