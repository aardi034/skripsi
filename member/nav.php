
<!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="../images/profil.jpg" width="80" height="80" alt="User Image">
        <div>
         <p class="app-sidebar__user-name">Marta Ardiyanto</p>
          <p class="app-sidebar__user-designation">Pejuang Skripsi</p> 
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="page-dashboard.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-database"></i><span class="app-menu__label">Master</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="page-iklan.php"><i class="icon fa fa-film"></i> Iklan</a></li>
          </ul>
            <ul class="treeview-menu">
            <li><a class="treeview-item" href="page-transaksi.php"><i class="icon fa fa-shopping-cart"></i> Transaksi</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-file-pdf-o"></i><span class="app-menu__label"> Report</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="laporan/report-iklan.php" target="_blank"><i class="icon fa fa-film"></i> Iklan</a></li>
            <li><a class="treeview-item" href="laporan/report-transaksi.php" target="_blank"><i class="icon fa fa-shopping-cart"></i> Transaksi</a></li>
          
          </ul>
        </li>
       
      </ul>
    </aside>
  
  
      