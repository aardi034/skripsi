<?php 

include 'fpdf.php';
include '../config/+koneksi.php';
class PDF extends FPDF
{
		function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Text color in gray
	    $this->SetTextColor(128);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}
}


$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf -> AddPage();
//header 
$pdf -> SetFont('Arial','B', 16);
$pdf -> Cell(0,5,'CENTRA BUDAYA SONOSEWU','0','1','C',false);
$pdf -> SetFont('Arial', 'B',12);
$pdf -> Cell(0,5,'CULTURE AND TOUR GUIDING','0','1','C',false);
$pdf -> Ln(3);
$pdf -> Cell(190,0.6,'','0','1','C',true);
$pdf -> Ln(5);
//judul
$pdf -> SetFont('Arial', 'B',9);
$pdf -> Cell(0,5,'Laporan Data Paket Perjalanan','0','1','C',false);
$pdf -> Ln(3);
for($i=1;$i<=40;$i++);
//kolom konten
$pdf -> SetFont('Arial', 'B',7);
$pdf -> Cell(15,6,'ID Paket',1,0,'C');
$pdf -> Cell(50,6,'Nama',1,0,'C');
// $pdf -> Cell(50,6,'Nama Kebudayaan',1,0,'C');
$pdf -> Cell(50,6,'Meeting Point',1,0,'C');
$pdf -> Cell(20,6,'Harga',1,0,'C');
$pdf -> Ln(2);

$query ='SELECT * FROM tb_paket ORDER BY idpk ASC';
$sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
while ($data= mysqli_fetch_array($sql_info)) {
//inner join
		// $idpk = $data['idpk'];
		// $query ="SELECT * FROM tb_detail_paket INNER JOIN tb_ctg_info ON tb_detail_paket.idi = tb_ctg_info.idi WHERE idpk = '$idpk' ";
		// $sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
		// $get_info= mysqli_fetch_array($sql_info);

	$pdf -> Ln(4);
	$pdf -> SetFont('Arial', '',7);
	$pdf -> Cell(15,4,$data['idpk'],1,0,'L');
	$pdf -> Cell(50,4,$data['nama_paket'],1,0,'L');
	// $pdf -> Cell(50,4,$get_info['nama'],1,0,'L');
	$pdf -> Cell(50,4,$data['kumpul'],1,0,'L');
	$pdf -> Cell(20,4,'Rp.'.$data['harga'],1,0,'R');

}

$pdf -> Output();
?>