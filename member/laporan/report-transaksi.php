<?php 

include 'fpdf.php';
include '../config/config.php';
session_start();

if(isset($_SESSION['username'])){
class PDF extends FPDF
{
		function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Text color in gray
	    $this->SetTextColor(128);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}
}


$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf -> AddPage();
//header 
$pdf -> SetFont('Arial','B', 16);
$pdf -> Cell(0,5,'LAPORAN ADMINISTRASI ','0','1','C',false);
$pdf -> SetFont('Arial', 'B',12);
$pdf -> Cell(0,5,'NGARGOYOSO MARKETPLACE','0','1','C',false);
$pdf -> Ln(3);
$pdf -> Cell(190,0.6,'','0','1','C',true);
$pdf -> Ln(5);
//judul
$pdf -> SetFont('Arial', 'B',9);
$pdf -> Cell(0,5,'Laporan Data Transaksi','0','1','C',false);
$pdf -> Ln(3);
for($i=1;$i<=40;$i++);
//kolom konten
$pdf -> SetFont('Arial', 'B',7);
$pdf -> Cell(10,6,'ID',1,0,'C');
$pdf -> Cell(30,6,'Nama Pembeli',1,0,'C');
$pdf -> Cell(20,6,'Alamat',1,0,'C');
$pdf -> Cell(30,6,'Barang',1,0,'C');
$pdf -> Cell(20,6,'Jumlah',1,0,'C');
$pdf -> Cell(20,6,'Total',1,0,'C');
$pdf -> Cell(35,6,'Status',1,0,'C');
$pdf -> Ln(2);

$username = $_SESSION['username'];
// 	$sql = mysqli_query($con," SELECT * FROM penjual INNER JOIN iklan ON penjual.id_penjual =  iklan.id_penjual WHERE username='$username' ORDER BY id_iklan DESC "); 
//                   $kategori   = mysqli_fetch_array($sql);
//                   $id_penjual = $kategori ['id_iklan'];

// $pdf -> Cell(35,6,$id_penjual,1,0,'C');

// $query ='SELECT * FROM transaksi A, detail_transaksi B, pembeli C, iklan D, penjual E WHERE A.id_detail = B.id_detail AND B.id_pembeli = C.id_pembeli AND B.id_iklan = D.id_iklan AND D.id_penjual = E.id_penjual AND E.id_penjual = "$id" ';

$query ="SELECT * FROM transaksi INNER JOIN detail_transaksi ON transaksi.id_detail = detail_transaksi.id_detail INNER JOIN pembeli ON detail_transaksi.id_pembeli = pembeli.id_pembeli INNER JOIN iklan ON detail_transaksi.id_iklan = iklan.id_iklan INNER JOIN penjual ON iklan.id_penjual = penjual.id_penjual WHERE penjual.username='$username' ";
$sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
while ($data= mysqli_fetch_array($sql_info)) {
//inner join
		// $idpk = $data['idpk'];
		// $query ="SELECT * FROM tb_detail_paket INNER JOIN tb_ctg_info ON tb_detail_paket.idi = tb_ctg_info.idi WHERE idpk = '$idpk' ";
		// $sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
		// $get_info= mysqli_fetch_array($sql_info);

	$pdf -> Ln(4);
	$pdf -> SetFont('Arial', '',7);
	$pdf -> Cell(10,4,$data['id_transaksi'],1,0,'L');
	$pdf -> Cell(30,4,$data['email'],1,0,'L');
	$pdf -> Cell(20,4,$data['alamat'],1,0,'L');
	$pdf -> Cell(30,4,$data['judul'],1,0,'L');
	$pdf -> Cell(20,4,$data['jumlah'],1,0,'R');
	$pdf -> Cell(20,4,$data['total'],1,0,'R');
	$pdf -> Cell(35,4,$data['status'],1,0,'L');
	
}
};
$pdf -> Output();
?>