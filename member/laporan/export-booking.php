<?php 

include 'fpdf.php';
include '../config/+koneksi.php';
class PDF extends FPDF
{
		function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Text color in gray
	    $this->SetTextColor(128);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}
}


$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf -> AddPage();
//header 
$pdf -> SetFont('Arial','B', 16);
$pdf -> Cell(0,5,'CENTRA BUDAYA SONOSEWU','0','1','C',false);
$pdf -> SetFont('Arial', 'B',12);
$pdf -> Cell(0,5,'CULTURE AND TOUR GUIDING','0','1','C',false);
$pdf -> Ln(3);
$pdf -> Cell(190,0.6,'','0','1','C',true);
$pdf -> Ln(5);
//judul
$pdf -> SetFont('Arial', 'B',9);
$pdf -> Cell(0,5,'Laporan Data Transaksi Pemesanan','0','1','C',false);
$pdf -> Ln(3);
for($i=1;$i<=40;$i++);
//kolom konten
$pdf -> SetFont('Arial', 'B',7);
$pdf -> Cell(15,6,'ID Trans',1,0,'C');
$pdf -> Cell(40,6,'Nama Member',1,0,'C');
$pdf -> Cell(30,6,'E-mail',1,0,'C');
$pdf -> Cell(20,6,'Tanggal',1,0,'C');
$pdf -> Cell(40,6,'Nama Paket',1,0,'C');
// $pdf -> Cell(50,6,'Nama Kebudayaan',1,0,'C');
$pdf -> Cell(50,6,'Id Status',1,0,'C');
$pdf -> Ln(2);

$query ='SELECT * FROM tb_booklist A, tb_detail_booklist B, tb_member C, tb_paket D, tb_status E WHERE A.idd = B.idd AND B.idm = C.idm AND B.idpk = D.idpk AND A.idst = E.idst' ;

// $query ='SELECT tb_booklist.idbl, tb_member.nama, tb_member.email, tb_detail_booklist.date_booking, tb_paket.nama, tb_status.keterangan FROM tb_booklist INNER JOIN tb_detail_booklist ON tb_booklist.idd = tb_detail_booklist.idd  INNER JOIN tb_member ON tb_detail_booklist.idm = tb_member.idm INNER JOIN tb_paket ON tb_detail_booklist.idpk = tb_paket.idpk INNER JOIN tb_status ON tb_booklist.idst = tb_status.idst';
$sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
while ($data= mysqli_fetch_array($sql_info)) {
//inner join
		// $idpk = $data['idpk'];
		// $query ="SELECT * FROM tb_detail_paket INNER JOIN tb_ctg_info ON tb_detail_paket.idi = tb_ctg_info.idi WHERE idpk = '$idpk' ";
		// $sql_info = mysqli_query($con, $query) or die( mysqli_error($con)); 
		// $get_info= mysqli_fetch_array($sql_info);

	$pdf -> Ln(4);
	$pdf -> SetFont('Arial', '',7);
	$pdf -> Cell(15,4,$data['idbl'],1,0,'L');
	$pdf -> Cell(40,4,$data['nama_member'],1,0,'L');
	$pdf -> Cell(30,4,$data['email'],1,0,'L');
	$pdf -> Cell(20,4,$data['date_booking'],1,0,'L');
	$pdf -> Cell(40,4,$data['nama_paket'],1,0,'L');
	$pdf -> Cell(50,4,$data['keterangan'],1,0,'L');
	
	

}

$pdf -> Output();
?>