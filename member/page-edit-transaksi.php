<?php
include 'header.php';
include 'nav.php';
include 'footer.php';

?>
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Ubah Status</h1>
    </div>
  </div>
  <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="row">
              <div class="col-lg-6">
                <?php
        require 'config/config.php';
        
           $id = $_GET['id'];
           $sql_t = mysqli_query($con, " SELECT * FROM transaksi WHERE id_transaksi ='$id' ") or die (mysqli_error($con));
           $data = mysqli_fetch_array($sql_t);
          ?>
                <form action="proses-edit-status.php" method="post">
                  <div class="form-group">
                    <label for="id">ID</label>
                    <input class="form-control" id="id" name="id" type="text" value="<?php echo $id?>" readonly>
                    <label for="kategori">Status Transaksi</label>
                  <select class="form-control" name="status"> 
                    <option value="<?=$data['status'];?>">Belum Diproses</option>
                    <option value="Proses Kirim">Proses Kirim</option>
                  </select>
                                </div>
            <div class="tile-footer">
              <input class="btn btn-primary" name="edit" value="simpan" type="submit">
            </div>
        </form>
        </div>
       </div>
      </div>
    </div>
  </div>
</div>
</main>