<?php
	require_once "config/config.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href=assets/font-awesome/css/font-awesome.min.css>
    <title>Login - Marketplace </title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Welcome</h1>
      </div>
      <div class="login-box">		
        <?php 
        session_start();
    				if(isset($_POST['login'])) {
    					$user = trim(mysqli_real_escape_string($con, $_POST['user']));
    					$pass = sha1(trim(mysqli_real_escape_string($con, $_POST['pass'])));
    					$sql_login = mysqli_query($con, "SELECT * FROM penjual WHERE username = '$user' AND password = '$pass'") or die (mysqli_error($con));
    					if(mysqli_num_rows($sql_login) > 0) {
    						$_SESSION['username'] = $user;
                            header('location:page-dashboard.php');
    					} else { 
                  echo"<script language='javascript'>alert('Username atau Password Salah !')</script>";?>
    					<?php
    				}
            }
    			?>

    			 <form class="login-form" action="" method="post">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
          <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input class="form-control" type="text" name="user" id="user" placeholder="Email" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="pass" id="pass" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox"><span class="label-text">Stay Signed in</span>
                </label>
              </div>
              <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
            </div>
          </div>
			<div class="input-group">
             <input type="submit" name="login" class="btn btn-primary btn-block" value="Login"></i>
					</div>
    			</form>
    		</div>
    	</div>
    </div><!-- 
    <script src="<?=base_url('assets/js/jquery.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>

 -->
</body>
</html>
